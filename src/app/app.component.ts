import { Component, OnInit } from '@angular/core';
import { LocationService } from './location.service';


// Marker Interface.
interface point {
  lat: number;
  lng: number;
  address: string;
  locationId: string;
  markerURL: {
    url: string,
    scaledSize: {
      width: number,
      height: number
    }
  }
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private locationService: LocationService) { }

  userMarker = {
    url: 'assets/shops.png',
    scaledSize: {
      width: 50,
      height: 50
    }
  }

  radius = 32000;

  zoom: number = 12;

  currentPos: point = {
    lat: 33.462,
    lng: -112.268,
    address: '',
    locationId: '',
    markerURL: this.userMarker
  };

  points: point[] = [];

  ngOnInit() {
    this.getLocations();
  }

  fetchMarkerDetails(position, action, index?) {
    const latlng = new google.maps.LatLng(position.lat, position.lng);
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'location': latlng }, (results, status) => {
      // This is checking to see if the Geoeode Status is OK before proceeding
      if (status == google.maps.GeocoderStatus.OK) {
        var address = (results[1].formatted_address);
        if (action == 'user') {
          this.currentPos.address = address;
        } else {
          this.points[index].address = address
        }
      }
    });
  }
  setRadius(radius) {
    this.radius = Number(radius);
    this.getLocations();
  }

  getLocations() {
    this.locationService.getTruckLocation(this.radius).subscribe(res => {
      const locationData = res.objects;
      this.points = [];
      locationData.forEach(location => {
        const marker = {
          url: location.id.includes('TWO-WHEELER') ? 'assets/bike.png' : 'assets/shipment.png',
          scaledSize: {
            width: 40,
            height: 40
          }
        }
        const params = {
          lat: location.object.coordinates[1],
          lng: location.object.coordinates[0],
          address: '',
          locationId: location.id,
          markerURL: marker
        };
        const isPointExist = this.points.some((point) => point.lat == params.lat && point.lng == params.lng);
        if (!isPointExist) {
          this.points.push(params);
        }
      });
    }, err => {
      this.getLocations();
    })
  }
}