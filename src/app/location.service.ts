import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient) { }

  getTruckLocation(radius): Observable<any> {
    return this.http.get(`https://a069-110-172-18-232.in.ngrok.io/v1/nearBy/`+radius);
  }
}